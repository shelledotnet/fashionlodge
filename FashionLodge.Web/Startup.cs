﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FashionLodge.Web.Startup))]
namespace FashionLodge.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

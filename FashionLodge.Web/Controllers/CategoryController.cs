﻿using FashionLodge.Database;
using FashionLodge.Database.UnitOfWorks;
using FashionLodge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FashionLodge.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly UnitOfWork utw;

        public CategoryController()
        {
            utw = new UnitOfWork(new FLContext());
        }

        // GET: Category
        public ActionResult Index()
        {
            try
            {
                var getAllOrerbyName = utw.Category.GetAll().OrderBy(a => a.Name);


                return View(getAllOrerbyName);
            }
            catch (Exception)
            {

                throw;
            }
            //finally
            //{
            //    if (utw != null)
            //    {
            //        utw.Dispose();
            //    }
            //}

        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "CreatedDate")]Category category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    utw.Category.Add(category);
                    utw.Complete();
                   
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw !=null)
                {
                    utw.Dispose();
                }
            }


        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                Category category = utw.Category.Get(id);
                if (category != null)
                {
                    return View(category);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }

        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(Category category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    utw.Category.Update(category);
                    utw.Category.PreventUpdate(category);
                    utw.Complete();
                  

                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }

        [HttpGet]
        public ActionResult Detail(int id)
        {
            try
            {
                Category category = utw.Category.Get(id);
                if (category != null)
                {
                    return View(category);
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                Category category = utw.Category.Get(id);
                if (category != null)
                {
                    return View(category);
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteCategory(int id)
        {
            try
            {
                Category category = utw.Category.Get(id);
                if (category != null)
                {
                    utw.Category.Remove(category);
                    utw.Complete();
                  
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }
    }
}
﻿using FashionLodge.Database;
using FashionLodge.Database.UnitOfWorks;
using FashionLodge.Entities;
using System;
using FashionLodge.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FashionLodge.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly UnitOfWork utw;

        public ProductController()
        {
            utw = new UnitOfWork(new FLContext());
        }

        // GET: Product
        public ActionResult Index()
        {
            try
            {

                // return View(utw.Product.GetAll());
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            //finally
            //{
            //    if (utw != null)
            //    {
            //        utw.Dispose();
            //    }
            //}

        }
        
        public ActionResult ProductTable(string searchs)
        {
            try
            {
                var getAllOrerbyName = utw.Product.GetAllByNameAsc();
                if (!string.IsNullOrEmpty(searchs))
                {
                     getAllOrerbyName = utw.Product.GetProductByName(searchs);

                    return PartialView(getAllOrerbyName);
                }

                return PartialView(getAllOrerbyName);

                // return PartialView(getAllOrerbyName);

            }
            catch (Exception)
            {

                throw;
            }
            //finally
            //{
            //    if (utw != null)
            //    {
            //        utw.Dispose();
            //    }
            //}

        }

        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "CreatedDate")]Product product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    utw.Product.Add(product);
                    utw.Complete();

                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }


        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                Product product = utw.Product.Get(id);
                if (product != null)
                {
                    return View(product);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }

        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(Product product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    utw.Product.Update(product);
                    utw.Product.PreventUpdate(product);
                    utw.Complete();


                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }

        [HttpGet]
        public ActionResult Detail(int id)
        {
            try
            {
                Product product = utw.Product.Get(id);
                if (product != null)
                {
                    return View(product);
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                Product product = utw.Product.Get(id);
                if (product != null)
                {
                    return View(product);
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteCategory(int id)
        {
            try
            {
                Product product = utw.Product.Get(id);
                if (product != null)
                {
                    utw.Product.Remove(product);
                    utw.Complete();

                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (utw != null)
                {
                    utw.Dispose();
                }
            }
        }
    }
}
﻿using FashionLodge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FashionLodge.Web.ViewModels
{
    public class ProductViewModel
    {
        public Product Product { get; set; }

        public List<Product> Products { get; set; }
    }
}
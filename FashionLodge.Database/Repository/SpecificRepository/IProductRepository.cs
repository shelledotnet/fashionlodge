﻿using FashionLodge.Database.Repository.GenericRepository;
using FashionLodge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FashionLodge.Database.Repository.SpecificRepository
{
  public  interface IProductRepository : IRepository<Product>
    {
        void PreventUpdate(Product product);

        IEnumerable<Product> GetAllByNameAsc();

        IEnumerable<Product> GetAllByNameDesc();

        IEnumerable<Product> GetProductByName(string name);
    }
}

﻿using FashionLodge.Database.Repository.GenericRepository;
using FashionLodge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FashionLodge.Database.Repository.SpecificRepository
{
  public  class ProductRepository : Repository<Product>,IProductRepository
    {
        public ProductRepository(FLContext fLContext)
            :base(fLContext)
        {

        }

        public IEnumerable<Product> GetAllByNameAsc()
        {
          return  DbSet.AsEnumerable()
                .OrderBy(a => a.Name)
                .ToList();
        }

        public IEnumerable<Product> GetAllByNameDesc()
        {
            return DbSet.AsEnumerable()
               .OrderByDescending(a => a.Name)
               .ToList();
        }


      public  IEnumerable<Product> GetProductByName(string name)
        {
            return DbSet.AsEnumerable()
                        .Where(a =>(a.Name !=null) && (a.Name.ToLower().Contains(name.ToLower())))
                        .ToList();
        }
        public void PreventUpdate(Product product)
        {
            _context.Entry<Product>(product).Property(a => a.CreatedDate).IsModified = false;
        }
    }
}

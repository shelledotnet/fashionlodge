﻿using FashionLodge.Database.Repository.GenericRepository;
using FashionLodge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FashionLodge.Database.Repository.SpecificRepository
{
  public  interface ICategoryRepository : IRepository<Category>
    {
        void PreventUpdate(Category category);
    }
}

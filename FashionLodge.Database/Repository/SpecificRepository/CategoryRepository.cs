﻿using FashionLodge.Database.Repository.GenericRepository;
using FashionLodge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FashionLodge.Database.Repository.SpecificRepository
{
  public  class CategoryRepository : Repository<Category>,ICategoryRepository
    {
        public CategoryRepository(FLContext fLContext):
            base(fLContext)
        {

        }

        public void PreventUpdate(Category category)
        {
            _context.Entry<Category>(category).Property(a => a.CreatedDate).IsModified = false;
        }
    }
}

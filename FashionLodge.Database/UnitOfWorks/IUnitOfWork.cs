﻿using FashionLodge.Database.Repository.SpecificRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FashionLodge.Database.UnitOfWorks
{
  public  interface IUnitOfWork : IDisposable
    {
         ICategoryRepository Category { get; }

         IProductRepository Product { get; }

        void Complete();

    }
}

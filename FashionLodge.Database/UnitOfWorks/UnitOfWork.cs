﻿using FashionLodge.Database.Repository.SpecificRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FashionLodge.Database.UnitOfWorks
{
  public  class UnitOfWork : IUnitOfWork
    {
        private readonly FLContext _fLContext;

        public UnitOfWork(FLContext flContext)
        {
            _fLContext = flContext;
            Category = new CategoryRepository(_fLContext);
            Product = new ProductRepository(_fLContext);

        }

     public   ICategoryRepository Category { get; private set; }

     public   IProductRepository Product { get; private set; }
        public void Complete()
        {
            _fLContext.SaveChanges();
        }

        public void Dispose()
        {
            _fLContext.Dispose();
        }
       
    }
}
